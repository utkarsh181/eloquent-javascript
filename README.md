# Eloquent JavaScript's Solutions

## Introduction

Eloquent JavaScript by Marijn Haverbeke is a book about JavaScript
programming language that focuses on writing real applications.

This project serves as a chapter wise collection of solution for its
exercises.

## Copying

Unless otherwise noted, all code herein is distributed under the terms
of the GNU General Public License Version 3 or later.


function flat(array, depth = 1) {
  if (depth == 0) return array;
  return flat(
    array.reduce((a, b) => a.concat(b), []),
    depth - 1
  );
}

function every(array, test) {
  for (const elem of array) {
    if (!test(elem)) return false;
  }
  return true;
}

function every2(array, test) {
  return !array.some(elem => !test(elem));
}

function loop(start, test, update, body) {
  let cur = start;
  while(test(cur)) {
    body(cur);
    cur = update(cur);
  }
}

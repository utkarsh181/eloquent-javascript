function elementByTagNames(node, tag) {
  let rslt = [];

  if (node == null) return rslt;

  for (let child of node.childNodes) {
    if (child.nodeName == tag.toUpperCase()) rslt.push(child);
    rslt = rslt.concat(elementByTagNames(child, tag));
  }
  return rslt;
}

class PGroup {
  constructor(items) {
    this.items = items;
  }

  add(item) {
    if (this.has(item)) return this;
    return new PGroup(this.items.concat(item));
  }

  delete(item) {
    if (!this.has(item)) return this;
    return new PGroup(this.items.filter((m) => m !== item));
  }

  has(item) {
    return this.items.includes(item);
  }

  *[Symbol.iterator]() {
    for (const item of this.items) yield item;
  }
}

PGroup.empty = new PGroup([]);

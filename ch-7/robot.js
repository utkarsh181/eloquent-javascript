class VillageState {
  constructor(place, parcels) {
    this.place = place;
    this.parcels = parcels;
  }

  move(roadGraph, destination) {
    if (!roadGraph.get(this.place).includes(destination)) {
      return this;
    } else {
      let parcels = this.parcels
        .map((p) => {
          if (p.place != this.place) return p;
          return { place: destination, address: p.address };
        })
        .filter((p) => p.place != p.address);

      return new VillageState(destination, parcels);
    }
  }

  static random(roadGraph, parcelCount = 5) {
    let parcels = [];

    for (let i = 0; i < parcelCount; i++) {
      let address = randomPick(roadGraph.keys(), roadGraph.size);
      let place;

      do {
        place = randomPick(roadGraph.keys(), roadGraph.size);
      } while (place == address);
      parcels.push({ place, address });
    }

    return new VillageState("Post Office", parcels);
  }
}

function addEdge(graph, from, to) {
  if (!graph.has(from)) {
    graph.set(from, [to]);
  } else {
    graph.set(from, graph.get(from).concat(to));
  }
}

function buildGraph(edges) {
  let graph = new Map();

  for (let [from, to] of edges.map((r) => r.split("-"))) {
    addEdge(graph, from, to);
    addEdge(graph, to, from);
  }
  return graph;
}

function randomPick(iter, length) {
  let choice = Math.floor(Math.random() * length);

  for (let rslt of iter) {
    if (choice == 0) return rslt;
    choice--;
  }
}

function randomRobot(roadGraph, state) {
  const adj = roadGraph.get(state.place);

  return { direction: randomPick(adj, adj.length) };
}

function routeRobot(roadGraph, state, memory) {
  const mailRoute = [
    "Alice's House",
    "Cabin",
    "Alice's House",
    "Bob's House",
    "Town Hall",
    "Daria's House",
    "Ernie's House",
    "Grete's House",
    "Shop",
    "Grete's House",
    "Farm",
    "Marketplace",
    "Post Office",
  ];

  if (memory.length == 0) {
    memory = mailRoute;
  }

  return { direction: memory[0], memory: memory.slice(1) };
}

function findRoute(roadGraph, from, to) {
  let work = [{ at: from, route: [] }];

  for (let i = 0; i < work.length; i++) {
    let { at, route } = work[i];

    for (let place of roadGraph.get(at)) {
      if (place == to) return route.concat(place);
      if (!work.some((w) => w.at == place)) {
        work.push({ at: place, route: route.concat(place) });
      }
    }
  }
}

function goalOrientedRobot(roadGraph, { place, parcels }, route) {
  if (route.length == 0) {
    let parcel = parcels[0];

    if (parcel.place != place) {
      route = findRoute(roadGraph, place, parcel.place);
    } else {
      route = findRoute(roadGraph, place, parcel.address);
    }
  }

  return { direction: route[0], memory: route.slice(1) };
}

function lazyRobot(roadGraph, { place, parcels }, route) {
  if (route.length == 0) {
    // Describe a route for every parcel
    let routes = parcels.map((parcel) => {
      if (parcel.place != place) {
        return {
          route: findRoute(roadGraph, place, parcel.place),
          pickUp: true,
        };
      } else {
        return {
          route: findRoute(roadGraph, place, parcel.address),
          pickUp: false,
        };
      }
    });

    // This determines the precedence a route gets when choosing.
    // Route length counts negatively, routes that pick up a package
    // get a small bonus.
    function score({ route, pickUp }) {
      return (pickUp ? 0.5 : 0) - route.length;
    }
    route = routes.reduce((a, b) => (score(a) > score(b) ? a : b)).route;
  }

  return { direction: route[0], memory: route.slice(1) };
}

function runRobot(roadGraph, state, robot, memory) {
  for (let turn = 0; ; turn++) {
    if (state.parcels.length == 0) {
      return turn;
    }
    let action = robot(roadGraph, state, memory);
    state = state.move(roadGraph, action.direction);
    memory = action.memory;
  }
}

function compareRobots(
  roadGraph,
  robot1,
  memory1,
  robot2,
  memory2,
  totalTask = 100
) {
  let total1 = 0;
  let total2 = 0;

  for (let i = 0; i < totalTask; i++) {
    let state = VillageState.random(roadGraph);
    total1 += runRobot(roadGraph, state, robot1, memory1);
    total2 += runRobot(roadGraph, state, robot2, memory2);
  }

  return [total1 / totalTask, total2 / totalTask];
}

function main() {
  const roads = [
    "Alice's House-Bob's House",
    "Alice's House-Cabin",
    "Alice's House-Post Office",
    "Bob's House-Town Hall",
    "Daria's House-Ernie's House",
    "Daria's House-Town Hall",
    "Ernie's House-Grete's House",
    "Grete's House-Farm",
    "Grete's House-Shop",
    "Marketplace-Farm",
    "Marketplace-Post Office",
    "Marketplace-Shop",
    "Marketplace-Town Hall",
    "Shop-Town Hall",
  ];
  const roadGraph = buildGraph(roads);

  console.log(compareRobots(roadGraph, goalOrientedRobot, [], lazyRobot, []));
}

main();

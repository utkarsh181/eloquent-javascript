class Group {
  constructor() {
    this.items = new Map();
  }

  add(item) {
    this.items.set(item, true);
  }

  delete(item) {
    this.items.delete(item);
  }

  has(item) {
    return this.items.has(item);
  }

  static from(iter) {
    let g = new Group();

    iter.forEach((x) => g.add(x));
    return g;
  }

  *[Symbol.iterator]() {
    for (const item of this.items.keys()) yield item;
  }
}

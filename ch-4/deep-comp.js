function deepEqual(val1, val2) {
  if (val1 === val2) return true;

  if (
    val1 == null ||
    val2 == null ||
    typeof val1 != "object" ||
    typeof val2 != "object"
  )
    return false;

  let keyVal1 = Object.keys(val1),
    keyVal2 = Object.keys(val2);

  if (keyVal1.length != keyVal2.length) return false;

  for (const key of keyVal1) {
    if (!deepEqual(val1[key], val2[key])) return false;
  }
  return true;
}

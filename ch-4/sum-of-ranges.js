function range(start, end, step = 1) {
  let rslt = [];

  if (step > 0) {
    while (start <= end) {
      rslt.push(start);
      start += step;
    }
  } else if (step < 0) {
    while (start >= end) {
      rslt.push(start);
      start += step;
    }
  }
  return rslt;
}

function sum(nums) {
  let rslt = 0;
  for (const num of nums) rslt += num;
  return rslt;
}

require("./journal.js");

function phi(table) {
  return (
    (table[3] * table[0] - table[2] * table[1]) /
    Math.sqrt(
      (table[2] + table[3]) *
        (table[0] + table[1]) *
        (table[1] + table[3]) *
        (table[0] + table[2])
    )
  );
}

function tableFor(event, journal) {
  let idx,
    table = [0, 0, 0, 0];
  for (let entry of journal) {
    idx = 0;
    if (entry.events.includes(event)) idx += 1;
    if (entry.squirrel) idx += 2;
    table[idx] += 1;
  }
  return table;
}

function journalEvents(journal) {
  let events = [];
  for (let entry of journal) {
    for (let event of entry.events) {
      if (!events.includes(event)) {
        events.push(event);
      }
    }
  }
  return events;
}

for (let event of journalEvents(JOURNAL)) {
  let correlation = phi(tableFor(event, JOURNAL));
  console.log(event + ":", correlation);
}
